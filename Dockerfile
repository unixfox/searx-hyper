from woahbase/alpine-searx:latest

RUN pip install -v --no-cache hyper

VOLUME /data

EXPOSE 8888

ENTRYPOINT ["/init"]